package www.s0ulless.com.listvieweg;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

public class ItemDetailActivity extends AppCompatActivity {

    private final String LOG_TAG = ItemDetailActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);

        // For information only.
        Log.i(LOG_TAG, "onCreate method called.");

        Intent receivedIntent = getIntent();
        // Checks if there is indeed an intent.
        if (receivedIntent != null) {
            String textFromIntent = receivedIntent.getStringExtra(BasicListViewActivity.CLICKED);
            // For information only.
            if (!TextUtils.isEmpty(textFromIntent)) {
                Log.i(LOG_TAG, textFromIntent);
            }
            else {
                Log.i(LOG_TAG, getString(R.string.empty_string));
            }

            String textToDisplay = "You clicked on " + textFromIntent + ".";
            // Sets TextView.
            TextView textView = (TextView) findViewById(R.id.detail_activity_text_view);
            textView.setText(textToDisplay);
        }
    }
}
