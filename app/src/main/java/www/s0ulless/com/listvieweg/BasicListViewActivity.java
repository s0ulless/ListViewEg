package www.s0ulless.com.listvieweg;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Basic ListView activity.
 */

public class BasicListViewActivity extends AppCompatActivity {

    private final String LOG_TAG = BasicListViewActivity.class.getName();
    static final String CLICKED = "intentExtraName";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // For information only.
        Log.i(LOG_TAG, "onCreate method called.");

        // Initializes the ArrayAdapter.
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, R.layout.list_item, R.id.list_item_text_view);
        // Adds strings from the arrays.xml file (res/values folder).
        arrayAdapter.addAll(getResources().getStringArray(R.array.mock_data));
        // Adds a single String from the strings.xml file (res/values folder).
        arrayAdapter.add(getString(R.string.manual_add));
        // Initializes the ListView and attaches the ArrayAdapter.
        ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // This is the text of the list item that was clicked.
                String text = arrayAdapter.getItem(position);
                // For information only. Viewable on Android Monitor, under 'Info'.
                Log.i(LOG_TAG, text);
                // https://developer.android.com/reference/android/content/Intent.html#Intent(android.content.Context, java.lang.Class<?>)
                Intent detailIntent = new Intent(BasicListViewActivity.this, ItemDetailActivity.class);
                // Puts clicked item into intent. In, this case it's just a String.
                // Retrieve this in the ItemDetailActivity.
                detailIntent.putExtra(CLICKED, text);
                startActivity(detailIntent);
            }
        });
    }
}
